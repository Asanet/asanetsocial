<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Asanet Social</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
	<center class="pt-4 mt-4">
		<h1 class="display-4">♥ Welcome To Our Family ♥</h1>
		<img src="https://media.giphy.com/media/zew17Cx5c136E/giphy.gif" alt="">
		<br>
		<a href="registration.php"><button class="btn btn-outline-primary">Join Us ☺</button></a>
	</center>
</body>
</html>