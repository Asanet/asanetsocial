<?php  include_once "layouts/header.php"; ?>


<div class="container">
  
  <div class="alert m-4" id="msg"></div>
  <h2 style="color: #87CEFA">Create your account</h2>
<form action="/action_page.php" class="col-md-4" style="background: #778899;border-radius: 5px">
  	<div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
    </div>
    <div class="form-group">
      <label for="surname">Surname:</label>
      <input type="text" class="form-control" id="surname" placeholder="Enter surname" name="surname">
    </div>
    <div class="form-group">
      <label for="date">Date of birth:</label>
      <input type="date" class="form-control" id="date" placeholder="Enter date" name="date">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="password">Password:</label>
      <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
    </div>
    
    <button type="button" class="btn btn-success"  id="save" style="margin-left:280px">Submit</button>
  </form>
</div>


<?php  include_once "layouts/footer.php"; ?>
<script type="text/javascript" src="assets/js/register.js"></script>